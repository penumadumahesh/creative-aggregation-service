package com.tcg.creativeaggregation.controller;

import com.tcg.creativeaggregation.model.CreativeAssetMapModel;
import com.tcg.creativeaggregation.service.CreativeAggregationService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/creativeAggregation")
public class CreativeAggregationController {

	@Autowired
	CreativeAggregationService creativeService;

	


	@ApiOperation(value = "Find an Creative Asset by Creative UUID", notes = "Get an Creative Asset by Creative UUID")
	@GetMapping("/{creativeUUID}")
	public ResponseEntity<CreativeAssetMapModel> findCreative(HttpServletRequest request,
			@PathVariable(name = "creativeUUID") String creativeuuid) throws Exception {
		
		return new ResponseEntity<CreativeAssetMapModel>(creativeService.findCreative(creativeuuid), HttpStatus.OK);
	}

	

}