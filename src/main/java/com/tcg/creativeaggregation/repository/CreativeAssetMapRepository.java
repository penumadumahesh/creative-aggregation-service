package com.tcg.creativeaggregation.repository;

import java.util.UUID;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tcg.creativeaggregation.entity.CreativeAssetMapEntity;


@Repository
public interface CreativeAssetMapRepository extends CrudRepository<CreativeAssetMapEntity, UUID> {

	@Query("select * from creative_asset_map where creative_uuid= :creative_uuid allow filtering")
	CreativeAssetMapEntity findCreativeByCreativeUuid(@Param("creative_uuid") UUID creative_uuid);
}
