package com.tcg.creativeaggregation.configuration;


import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
	@Bean
	public Docket api() {

		Parameter accessTokenParam = new ParameterBuilder()
                .name("Authorization")
                .description("Authentication Token")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .build();

        List<Parameter> params = new ArrayList<Parameter>();
        params.add(accessTokenParam);

		return new Docket(DocumentationType.SWAGGER_2)
				.globalOperationParameters(params)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.regex("/creativeAggregation.*"))
				.build()
				.apiInfo(new ApiInfo("Creative Aggregation Service", "Spring Boot Rest API for Creative Aggregation Service", "1.0", "Terms of Service",
						new Contact("Tru Clear Global", "http://truclearglobal.net", ""),
						"TruClearGlobal License Version 1.0", "http://truclearglobal.net",new ArrayList<>()))
				;
	}

	
}