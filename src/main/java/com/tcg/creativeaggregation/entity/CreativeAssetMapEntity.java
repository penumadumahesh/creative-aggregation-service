package com.tcg.creativeaggregation.entity;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;


import java.util.UUID;


@Table(value = "creative_asset_map")
public class CreativeAssetMapEntity {

    
	@PrimaryKeyColumn(name = "creative_uuid", type = PrimaryKeyType.PARTITIONED)
	private UUID creativeUUID;
	
	@PrimaryKeyColumn(name = "asset_uuid", type = PrimaryKeyType.PARTITIONED)
	private UUID assetUUID;
	
	@Column("client_uuid")
	private UUID clientUUID;
	
	@Column("installtion_uuid")
	private UUID installationUUID;

	public UUID getCreativeUUID() {
		return creativeUUID;
	}

	public void setCreativeUUID(UUID creativeUUID) {
		this.creativeUUID = creativeUUID;
	}

	public UUID getAssetUUID() {
		return assetUUID;
	}

	public void setAssetUUID(UUID assetUUID) {
		this.assetUUID = assetUUID;
	}

	public UUID getClientUUID() {
		return clientUUID;
	}

	public void setClientUUID(UUID clientUUID) {
		this.clientUUID = clientUUID;
	}

	public UUID getInstallationUUID() {
		return installationUUID;
	}

	public void setInstallationUUID(UUID installationUUID) {
		this.installationUUID = installationUUID;
	}
	
	
}
