package com.tcg.creativeaggregation.service;


import com.tcg.creativeaggregation.model.CreativeAssetMapModel;


public interface CreativeAggregationService {
	CreativeAssetMapModel findCreative(String creativeuuid);

}
