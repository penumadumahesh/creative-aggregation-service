package com.tcg.creativeaggregation.service.impl;

import com.tcg.common.exceptions.TCGErrorCode;
import com.tcg.common.exceptions.TCGException;
import com.tcg.creativeaggregation.entity.CreativeAssetMapEntity;
import com.tcg.creativeaggregation.model.CreativeAssetMapModel;
import com.tcg.creativeaggregation.repository.CreativeAssetMapRepository;
import com.tcg.creativeaggregation.service.CreativeAggregationService;


import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreativeAggregationServiceImpl implements CreativeAggregationService {
	
	@Autowired
	CreativeAssetMapRepository creativeRepository;


	@Override
	public CreativeAssetMapModel findCreative(String creativeuuid) {
		CreativeAssetMapEntity creative = creativeRepository.findCreativeByCreativeUuid(UUID.fromString(creativeuuid));
		if (creative == null) {
			throw new TCGException("No Creative Asset found", TCGErrorCode.ITEM_NOT_FOUND);
		}
		CreativeAssetMapModel creativeModel = new CreativeAssetMapModel();
		BeanUtils.copyProperties(creative, creativeModel);
		return creativeModel;
	}


}
