package com.tcg.creativeaggregation.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.UUID;


@JsonPropertyOrder(alphabetic = true)
public class CreativeAssetMapModel {

	@JsonProperty(value = "creativeUUID")
	private UUID creativeUUID;
	
	@JsonProperty(value ="assetUUID")
	private UUID assetUUID;
	
	@JsonProperty(value ="clientUUID")
	private UUID clientUUID;
	
	@JsonProperty(value ="installationUUID")
	private UUID installationUUID;

	public UUID getCreativeUUID() {
		return creativeUUID;
	}

	public void setCreativeUUID(UUID creativeUUID) {
		this.creativeUUID = creativeUUID;
	}

	public UUID getAssetUUID() {
		return assetUUID;
	}

	public void setAssetUUID(UUID assetUUID) {
		this.assetUUID = assetUUID;
	}

	public UUID getClientUUID() {
		return clientUUID;
	}

	public void setClientUUID(UUID clientUUID) {
		this.clientUUID = clientUUID;
	}

	public UUID getInstallationUUID() {
		return installationUUID;
	}

	public void setInstallationUUID(UUID installationUUID) {
		this.installationUUID = installationUUID;
	}

}
