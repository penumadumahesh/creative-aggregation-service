package com.tcg.creativeaggregation;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages= {"com.tcg.creativeaggregation.*", "com.tcg.common.*"})
public class CreativeAggregationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreativeAggregationApplication.class, args);
	}

}
