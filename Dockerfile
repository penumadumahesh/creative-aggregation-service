#Docker file
FROM frolvlad/alpine-oraclejdk8:slim
ENV KEYSPACENAME $KEYSPACENAME
ENV PORT 9031
ENV ACCESSKEY $ACCESSKEY
ENV SECRETKEY $SECRETKEY
ENV REGION $REGION
ENV DATABASENAME $DATABASENAME
ENV EUREKA_URI $EUREKA_URI
ENV KAFKA_SERVER $KAFKA_SERVER
ENV STORAGESERVICEURL $STORAGESERVICEURL
ENV STORAGESITE $STORAGESITE
ENV JAVA_OPTS="-Xmx128m -Xss256k"
VOLUME /tmp
ADD target/creative-aggregation-service.jar creative-aggregation-service.jar
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar creative-aggregation-service.jar" ]
